describe('calculatrice', function() {
    var expect = require('chai').expect;
    var app = require('../www/js/src/fonctions-calculatrice.js');
    describe('calcul taux de remise', function() {
        it('calcul le taux de remise avec deux nombre entiers, renvoi un entier', function() {
            expect(app.calculeTauxDeRemise(20, 10)).to.equal(50.00);
        })
        it('le calcul du taux de remise avec deux nombre entiers, renvoi un décimal', function() {
             expect(app.calculeTauxDeRemise(102, 75)).to.equal(26.47);
        })
        it('le calcul du taux de remise avec deux nombre décimaux, renvoi un décimal', function() {
             expect(app.calculeTauxDeRemise(10.5, 7.5)).to.equal(28.57);
        })
        it('taux de remise = 0, si le prix d achat brut = 0', function() {
            expect(app.calculeTauxDeRemise(0, 10)).to.equal(0);
        })
        it('taux de remise = 0, si le prix d achat brut = null', function() {
         expect(app.calculeTauxDeRemise(null, 25)).to.equal(0);
        })
        it('renvoi message d erreur si le taux de remise < 0', function() {
         expect(app.calculeTauxDeRemise(0, 25)).to.equal(0);
        })
    }),
    describe('calcul prix achat net', function() {
        it('calcul du prix d achat net avec deux entiers, renvoi un entier', function() {
            expect(app.calculePrixAchatNet(50.00, 20)).to.equal(10.00);
        })
        it('calcul du prix d achat net avec avec deux entiers, renvoi un décimal', function() {
            expect(app.calculePrixAchatNet(25.00, 18)).to.equal(13.50);
        })
        it('calcul du prix d achat net avec avec deux décimaux, renvoi un décimal', function() {
            expect(app.calculePrixAchatNet(33.33, 20.50)).to.equal(13.67);
        })
        it('prix d achat net = prix d achat brut, si le taux de remise = null', function() {
            expect(app.calculePrixAchatNet(null, 20)).to.equal(20);
        })
        it('prix d achat net = 0, si le prix d achat brut = null', function() {
            expect(app.calculePrixAchatNet(20, null)).to.equal(0);
        })
        it('prix d achat net = prix d achat brut, si le taux de remise = 0', function() {
            expect(app.calculePrixAchatNet(0.00, 20)).to.equal(20);
        })
        it('renvoi message d erreur si le taux de remise < 0', function() {
            expect(app.calculePrixAchatNet(-1.00, 20)).to.equal("Attention ! Les valeurs doivent être supérieures à 0");
        })
    }),
    describe('calcule prix de vente net', function() {
        it('calcul du prix de vente net avec deux entiers, renvoi un entier', function() {
            expect(app.calculePrixVenteNet(10, 2)).to.equal(20.00);
        })
        it('calcul du prix de vente net avec prix achat net = un décimal, renvoi un entier', function() {
            expect(app.calculePrixVenteNet(10.5, 2)).to.equal(21.00);
        })
        it('calcul du prix de vente net avec prix achat net = un décimal, renvoi un décimal', function() {
            expect(app.calculePrixVenteNet(10.25, 2)).to.equal(20.50);
        })
        it('prix de vente net = 0, si le coeff = null', function() {
            expect(app.calculePrixVenteNet(10, null)).to.equal(10);
        })
        it('prix de vente net = 0, si le prix d achat net = null', function() {
            expect(app.calculePrixVenteNet(null, 2)).to.equal(0);
        })
        it('prix de vente net = prix d achat net, si le coeff = 0', function() {
            expect(app.calculePrixVenteNet(10, 0)).to.equal(10);
        })
        it('renvoi message d erreur si le coeff < 0', function() {
            expect(app.calculePrixVenteNet(10, -2)).to.equal("Attention ! Les valeurs doivent être supérieures à 0");
        })
    }),
    describe('calcul coefficent multiplicateur', function() {
        it('calcul du coefficent multiplicateur avec deux nombre entiers', function() {
            expect(app.calculeCoefficientMultiplicateur(20, 40)).to.equal(2.00);
        })
        it('calcul du coefficent multiplicateur avec deux décimaux', function() {
         expect(app.calculeCoefficientMultiplicateur(10, 15)).to.equal(1.5);
        })
        it('le calcul du coefficent multiplicateur renvoie un décimal', function() {
            expect(app.calculeCoefficientMultiplicateur(30, 100)).to.equal(3.33);
        })
        it('coefficient multiplicateur = 0, si le prix d achat net = 0', function() {
            expect(app.calculeCoefficientMultiplicateur(0, 10)).to.equal(0);
        })
        it('coefficient multiplicateur= 0, si le prix d achat net = null', function() {
         expect(app.calculeCoefficientMultiplicateur(null, 25)).to.equal(0);
        })
        it('renvoi message d erreur si le prix d achat net < 0', function() {
         expect(app.calculeCoefficientMultiplicateur(0, 25)).to.equal(0);
        })
    })
  })