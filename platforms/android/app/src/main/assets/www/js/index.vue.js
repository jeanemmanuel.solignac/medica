var tauxRemise = new Vue({
  el: '#taux-remise',
  data: {
    achatNet: 0,
    achatBrut: 0
  },
  computed: {
    result: function () {
      if (this.achatBrut !== 0 && this.achatBrut != null) {
        var num = ((1 - this.achatNet) / this.achatBrut) * 100
        return num.toFixed(2)
      }
      return 0;
    }
  }
})
