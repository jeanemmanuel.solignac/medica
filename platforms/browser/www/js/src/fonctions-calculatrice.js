function calculeTauxDeRemise(achatBrut, achatNet) {
    if (achatBrut !== 0 && achatBrut != null) {
        if (achatBrut > 0 && achatNet > 0) {
            var num = (1 - (achatNet / achatBrut)) * 100
            return parseFloat(num.toFixed(2))
        }
        else {
            return "Attention ! Les valeurs doivent être supérieures à 0"
        }
      }
      return 0;
}

function calculePrixAchatNet(tauxRemise, achatBrut) {
    if (tauxRemise !== 0 && tauxRemise != null) {
        if (achatBrut !== 0 && achatBrut != null) {
            if (tauxRemise > 0 && achatBrut > 0) {
                var num = achatBrut * ((100 - tauxRemise) / 100)
                return parseFloat(num.toFixed(2))
            }
            else {
                return "Attention ! Les valeurs doivent être supérieures à 0"
            }
        }
        else {
            return 0;
        }
    }
    return achatBrut;
}

function calculePrixVenteNet(achatNet, coeff) {
    if (coeff !== 0 && coeff != null) {
        if (achatNet !== 0 && achatNet != null) {
            if (achatNet > 0 && coeff > 0) {
                var num = achatNet * coeff
                return parseFloat(num.toFixed(2))
            }
            else {
                return "Attention ! Les valeurs doivent être supérieures à 0"
            }
        }
        else {
            return 0;
        }
    }
    return achatNet;
}

function calculeCoefficientMultiplicateur(achatNet, venteNet) {
    if (achatNet !== 0 && achatNet != null) {
        if (achatNet > 0 && venteNet > 0) {
            var num = venteNet / achatNet
            return parseFloat(num.toFixed(2))
        }
        else {
            return "Attention ! Les valeurs doivent être supérieures à 0"
        }
    }
    return 0;
}

if (typeof module !== 'undefined') {
  module.exports = {
    calculeCoefficientMultiplicateur: calculeCoefficientMultiplicateur,
    calculePrixAchatNet : calculePrixAchatNet,
    calculePrixVenteNet : calculePrixVenteNet,
    calculeTauxDeRemise : calculeTauxDeRemise
  }
}
