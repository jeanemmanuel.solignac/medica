var tauxRemise = new Vue({
  el: '#taux-remise',
  data: {
    achatNet: null,
    achatBrut: null
  },
  computed: {
    result: function () {
      return calculeTauxDeRemise(this.achatBrut, this.achatNet);
    }
  }
})

var prixAchatNet = new Vue({
    el: '#prix-achat-net',
    data: {
        achatBrut: null,
        tauxRemise: null
    },
    computed: {
        result: function () {
            return calculePrixAchatNet(this.tauxRemise, this.achatBrut);
        }
    }
})

var pvNet = new Vue({
    el: '#pv-net',
    data: {
        achatNet: null,
        coeff: null
    },
    computed: {
        result: function () {
            return calculePrixVenteNet(this.achatNet, this.coeff);
        }
    }
})

var coefMultiplicateur = new Vue({
    el: '#coef-multiplicateur',
    data: {
        venteNet: null,
        achatNet: null
    },
    computed: {
        result: function () {
            return calculeCoefficientMultiplicateur(this.achatNet, this.venteNet);
        }
    }
})
