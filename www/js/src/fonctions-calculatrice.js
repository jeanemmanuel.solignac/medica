function calculeTauxDeRemise(achatBrut, achatNet) {
    if (achatBrut !== 0 && achatBrut != null) {
        var num = (1 - (achatNet / achatBrut)) * 100
        return num.toFixed(2)
      }
      return 0;
}

function calculePrixAchatNet(tauxRemise, achatBrut) {
    if (tauxRemise !== 0 && tauxRemise != null) {
        var num = achatBrut * ((100 - tauxRemise) / 100)
        return num.toFixed(2)
    }
    return 0;
}

function calculePrixVenteNet(achatNet, coeff) {
    if (achatNet !== 0 && achatNet != null) {
        var num = achatNet * coeff
        return num.toFixed(2)
    }
    return 0;
}

function calculeCoefficientMultiplicateur(achatNet, venteNet) {
    if (achatNet !== 0 && achatNet != null) {
        var num = venteNet / achatNet
        return num.toFixed(2)
    }
    return 0;
}