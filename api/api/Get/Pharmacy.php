<?php

namespace Api\Get;

use \Api\Database;

/**
 * Echo a JSON string containing a pharmacy's data
 */
class Pharmacy
{
    public function __construct(array $vars)
    {
        $database = Database::getInstance();
        $request = $database->getPDO()->prepare(
            'SELECT * FROM pharmacy
            WHERE id = :id'
        );
        $request->execute([
            'id' => $vars['id']
        ]);

        echo \json_encode($request->fetch());
    }
}
