<?php

namespace Api\Get;

use \Api\Database;

/**
 * Echo a JSON string containing all pharmacies's data
 */
class Pharmacies
{
    public function __construct()
    {
        $database = Database::getInstance();
        $request = $database->getPDO()->query(
            'SELECT * FROM pharmacy'
        );

        echo \json_encode($request->fetchAll());
    }
}
