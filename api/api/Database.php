<?php

namespace Api;

class Database
{
    private $host;
    private $database;
    private $username;
    private $password;
    private $pdo;

    private function __construct()
    {
        $this->host = 'db5000119414.hosting-data.io';
        $this->database = 'dbs114081';
        $this->username = 'dbu209677';
        $this->password = 'MsprEpsi2019:/';

        $this->pdo = new \PDO(
            "mysql:host=$this->host; dbname=$this->database;",
            $this->username,
            $this->password
        );
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        $this->createIfNotExist();
    }

    public static function getInstance(): Database
    {
        static $instance;

        if ($instance == null) {
            return new Database();
        }

        return $instance;
    }

    private function createIfNotExist(): void
    {
        // Création de la table pharmacy
        $this->pdo->query('
        CREATE TABLE IF NOT EXISTS pharmacy(
            id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
            name VARCHAR(40),
            address VARCHAR(100),
            code INT,
            city VARCHAR(40),
            sells INT,
            needle VARCHAR(40)
        );
        ');
    }

    public function getPDO(): \PDO
    {
        return $this->pdo;
    }

    private function __clone()
    {

    }
}
