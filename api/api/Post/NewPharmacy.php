<?php

namespace Api\Post;

use \Api\Database;

/**
 * Create and insert a new pharmacy
 */
class NewPharmacy
{
    private $columns;

    public function __construct()
    {
        $this->columns = [
            'name',
            'address',
            'code',
            'city',
            'sells',
            'needle'
        ];

        if (isComplete()) {
            $database = new Database();
            $request = $database->getPDO()->prepare(
                'INSERT INTO pharmacy(name, address, code, city, sells, needle)
                VALUES(:name, :address, :code, :city, :sells, :needle)';
            );

            $request->execute([
                'name' => $_POST['name'],
                'address' => $_POST['address'],
                'code' => $_POST['code'],
                'city' => $_POST['city'],
                'sells' => $_POST['sells'],
                'needle' => $_POST['needle']
            ]);
        }
    }

    /**
     * Indicate if all post variables are defined or not
     * @return bool True if all variables are defined
     */
    private function isComplete(): bool
    {
        foreach ($this->columns as $column) {
            if (!isset($_POST[$column])) {
                return false;
            }

            return true;
        }
    }
}
