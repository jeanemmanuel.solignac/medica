<?php

require_once __DIR__ . '/vendor/autoload.php';



$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) {
    // Get a pharmacy's data
    $r->addRoute('GET', '/pharmacy/{id:\d+}', 'Api\\Get\\Pharmacy');

    // Edit a pharmacy's data
    $r->addRoute('POST', '/pharmacy/{id:\d+}', 'Api\\Post\\Pharmacy');

    // Get all pharcies'data
    $r->addRoute('GET', '/pharmacies', 'Api\\Get\\Pharmacies');

    // Insert a new pharmacy
    $r->addRoute('POST', '/pharmacy', 'Api\\Post\\Pharmacy');

    // Delete a pharmacy
    $r->addRoute('DELETE', '/pharmacy/{id:\d+}', 'Api\\Delete\Pharmacy');
});

// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);
switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        http_response_code(404);
        break;

    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        http_response_code(405);
        break;

    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];
        new $handler($vars);
        break;
}
